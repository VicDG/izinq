<?php
/**
 * This file will be called from the browser to push xml files set in the configuration
 * to an external server
 *
 * PHP version 5.3
 *
 * @author     Vic Degraeve <vicdegraeve@me.com>
 * @version    0.1
 */

// Browser support
header("Content-Type:text/plain");

// Load up config file 
$ini = parse_ini_file(implode('/', array_slice(explode("/", __FILE__), 0, -2)).'/config.ini');

// Hardcode constants
define('SCRIPTPATH', dirname(__FILE__));
define('PUSH', $ini['push_directory']);
define('PUSHLOG', $ini['push_log_file']);
require_once(SCRIPTPATH . '/' . 'lib' . '/' . 'push.php');

// Loop through files in push directory, push each of them
$p = new Push;
$xmls = glob(PUSH . '/' . '*.xml');
if($xmls === FALSE) $xmls = Array();
$log = Array();

foreach ($xmls as $xml) {
  $basename = basename($xml);
  echo "Current time: ".date('d-m-Y H:i:s')."\n";
  echo "Starting for file: $basename\n";
  // Request
  $r = $p->securewebregistrations(file_get_contents($xml), false);
  $status = 'Error'; 

  // We pushed it
  if($r) {
    echo "The XML file was successfully pushed to the server.\n";
    unlink($xml);
    echo "XML file deleted from push folder\n";
    $status = 'Sent';
  }
  else {
    echo "Couldn't reach external server.\n";
    $status = 'Server offline';
  }
  
  // Done with this file
  echo "--- Leaving $basename ---\n"; 
  $time = date('d-m-Y H:i:s');
  array_push($log, array($time, $basename, $status));
  sleep(1);
}

$newfile = (filesize(PUSHLOG)) ? false : true;
$loghandle = fopen(PUSHLOG, 'a');
if($newfile) fputcsv($loghandle, array('Time', 'Filename (in PUSH directory)', 'Status'));
foreach ($log as $entries) {
  fputcsv($loghandle, $entries); 
}
fclose($loghandle);
