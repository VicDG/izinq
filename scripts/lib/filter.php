<?php
/**
 * This class will implement the filtering language
 *
 * PHP version 5.3
 *
 * @author     Vic Degraeve <vicdegraeve@me.com>
 * @version    1.0
 */

class Filter {  
  private function open($path) {
    if(file_exists($path)) {
      $content = file_get_contents($path);
      return explode("\n", $content);
    }
    else {
      return FALSE;
    }
  }
  
  private function loop($lines) {
    $statements = Array();
    $iftexts = Array();
    if($lines) {
      foreach ($lines as $key => $line) {
        if(in_array($line, $statements)) {
          // Do nothing
        }
        elseif(trim($line) == 'pass' || trim($line) == 'block' || trim($line) == 'push' || stripos(trim($line), 'ignore') === 0 || stripos(trim($line), 'exclude') === 0) {
          $from_start = $key;
          $ignore = false;
          if(stripos(trim($line), 'ignore') === 0 || stripos(trim($line), 'exclude') === 0) {
            $words = (explode(' ', trim($line)));
            $ignore = trim(implode(' ', array_slice($words, 1)));
            $line = $words[0];
          }

          foreach (array_slice($lines, $key, count($lines), true) as $xkey => $xline) {
            if(trim($xline) == 'end') {
              $endkey = $xkey;
              break;
            }
          }
    
          $from_end = ( count($lines) -  ($endkey + 1 ) );
          $statements = array_slice($lines, $from_start + 1, ('-' . ($from_end + 1)), true); 
    
          // Call parser
          array_push($iftexts, Array('(' . $this->parse($statements) . ')', trim($line), $ignore));
        }
      }
      return $iftexts;
    }
    else {
      return FALSE;
    }
  }
  
  public function parse($lines) {
    $iftext = '';
    foreach ($lines as $line) {
      if($iftext !== '') $iftext .= ' && ';
      $line = trim($line);
      $words = explode(' ', $line);

      $subject = $words[0];
      $subject_object = '$s_address->$legend[\''.$subject.'\']';

      array_shift($words);

      // Check for BUT DOES NOT
      foreach ($words as $key => $word) {
        if(strtoupper($word) == 'DOES' && strtoupper($words[$key+1]) == 'NOT' && strtoupper($words[$key+2]) == 'CONTAIN') {
          array_splice($words, $key, 3, Array('DOES NOT CONTAIN'));
        }
      }

      // Trim ""
      foreach ($words as $key => $word) {
        if((strpos($word, '"') === 0) && (strrpos($word, '"') === strlen($word) - 1)) {
          $words[$key] = trim($word, '"');
        }
      }
      
      // Check for ""
      foreach ($words as $key => $word) {
        if(strpos($word, '"') === 0) {
          foreach (array_slice($words, $key, count($words), true) as $xkey => $xword) {
            if(strrpos($xword, '"') === strlen($xword) - 1) {
              $endkey = $xkey;
              break;
            }
          }
          
          $from_start = $key;
          $from_end = '-' . ( count($words) -  ($endkey + 1 ) );
      
          if($from_end == '-0') {
            $from_end = count($words);
          }
      
          $quotecontent = array_slice($words, $from_start, $from_end);
          $emptyarray = Array();
          $amt = (count($quotecontent) - 1);
          for ($i = 0; $i < $amt; $i++) {
            array_push($emptyarray, '');
          }

          array_push($emptyarray, trim(implode(' ', $quotecontent), '"'));
          $replace = $emptyarray;
          array_splice($words, $from_start, $from_end, $replace); 
        }
      }
      $words = array_filter($words, 'strlen');
    
      // Check action
      if(strtoupper($words[0]) == 'IS') {
        array_shift($words);

        if(strtoupper($words[0]) === 'NOT') {
          $operator = ' != ';
          array_shift($words);
        }
        else {
          $operator = ' == ';
        }

        $iftext .= '(strtolower(' . $subject_object . ') ' . $operator . '"' . addslashes(strtolower(trim($words[0]))) . '"' . ')';
        array_shift($words);
        foreach ($words as $word) {
          if(!(trim(strtolower($word)) == 'or')) {
            $iftext .= ' || (strtolower(' . $subject_object . ') ' . $operator . '"' . addslashes(strtolower(trim($word))) . '"' . ')';
          }
        }
      }

      if(strtoupper($words[0]) == 'CONTAINS') {
        $subif = '(';
        array_shift($words);
        $contains = Array();
        foreach ($words as $word) {
          if(strtolower($word) == 'and') {
            $subif .= ' && ';
          }
          elseif(strtolower($word) == 'or') {
            $subif .= ' || ';
          }
          else {
            $subif .= 'strpos(strtolower(' . $subject_object . '), "' . addslashes(strtolower($word)) . '") !== false';
          }
        }
        $subif .= ')';
        $iftext .= $subif;
      }


      if(strtoupper($words[0]) == 'DOES NOT CONTAIN') {
        $subif = '(';
        array_shift($words);
        $contains = Array();
        foreach ($words as $word) {
          if(strtolower($word) == 'and') {
            $subif .= ' && ';
          }
          elseif(strtolower($word) == 'or') {
            $subif .= ' && ';
          }
          else {
            $subif .= 'strpos(strtolower(' . $subject_object . '), "' . addslashes(strtolower($word)) . '") === false';
          }
        }
        $subif .= ')';
        $iftext .= $subif;
      }
    }

    return $iftext; 
  }

  public function checkFilter($path, $s_address, $legend) {
    $lines = $this->open($path);
    $results = $this->loop($lines);

    $blockevals = Array();
    $passevals = Array();
    $excludeevals = Array();
    $ignoreevals = Array();
    $pushevals = Array();
    foreach ($results as $result) {
      if($result[1] == 'pass') {
        array_push($passevals, $result[0]);
      }
      elseif($result[1] == 'block') {
        array_push($blockevals, $result[0]);
      }
      elseif($result[1] == 'exclude') {
        array_push($excludeevals, $result[0]);
        $exclude = $result[2];
      }
      elseif($result[1] == 'ignore') {
        array_push($ignoreevals, $result[0]);
        $ignore = $result[2];
      }
      elseif($result[1] == 'push') {
        array_push($pushevals, $result[0]);
      }
    }
    $blockevals = implode(' || ', $blockevals);
    $passevals = implode(' || ', $passevals);
    $excludeevals = implode(' || ', $excludeevals);
    $ignoreevals = implode(' || ', $ignoreevals);
    $pushevals = implode(' || ', $pushevals);
    
    if(isset($exclude)) {
      if(stripos($exclude, ' and ') !== false) {
        $exclude = explode(' and ', trim($exclude));
      }
      else {
        $exclude = Array(trim($exclude));
      }
    }
    /* 
      We can't use this return technique anymore, as we may have to supply more than one flag at a time. We're going for a multidimensional Array approach instead.
      It should not slow down the script, nor overcomplicate the process-side implementation.

      Old implementation:
      if($blockevals != '' && eval('if('.$blockevals.') return true;') === true) {
        return Array('block');
      }
      elseif($passevals != '' && eval('if('.$passevals.') return true;') === true) {
        return Array('pass');
      }
      elseif($excludeevals != '' && eval('if('.$excludeevals.') return true;') === true) {
        return Array('exclude', $exclude);
      }
      elseif($ignoreevals != '' && eval('if('.$ignoreevals.') return true;') === true) {
        return Array('ignore', $ignore);
      }
      else {
        return false;
      }
    */
    
    $return = Array();
    if($blockevals != '' && eval('if('.$blockevals.') return true;') === true) {
      $return['block'] = TRUE;
    }
    if($passevals != '' && eval('if('.$passevals.') return true;') === true) {
      $return['pass'] = TRUE;
    }
    if($excludeevals != '' && eval('if('.$excludeevals.') return true;') === true) {
      $return['exclude'] = $exclude;
    }
    if($ignoreevals != '' && eval('if('.$ignoreevals.') return true;') === true) {
      $return['ignore'] = $ignore;
    }
    if($pushevals != '' && eval('if('.$pushevals.') return true;') === true) { 
      $return['push'] = TRUE;
    }
    return $return; 
  }
}
