<?
/**
 * This class will handle number splitting
 *
 * PHP version 5.3
 *
 * @author     Vic Degraeve <vicdegraeve@me.com>
 * @version    1.0
 */

class Number {
  public function split($street) {
    $pattern = "#^([a-z0-9 \p{L} [:punct:] \']*) ([0-9]{1,5}[a-z0-9 \-\/]{0,})$#iu";
    $street = trim($street);
    $street = str_replace("\n", ' ', $street);
    
    // Check if there is an appartment denoted in the beginning of the address
    $appartmentpattern = "/^((Appartment|Apt.|Apartment|Appt|Appt.|Appartement|Apt|#)\.?\s*[0-9]+)/iu";
    if(preg_match($appartmentpattern, $street, $matches)) {
      $street = trim(preg_replace($appartmentpattern, '', $street));
      $additional = trim($matches[1]);
    }
    
    // Match that sucker
    if(preg_match("#^([0-9])#", $street) && !preg_match("#^([0-9]{0,5}[(\p{L})a-zA-z]{4,})#iu", $street)) {
      $pattern = "#^([0-9]*[a-z]*[[-]*[0-9]+[a-z]*]*)[,]?[\s]*([a-z0-9\-\/ \'\.\#[:punct:] \p{L}]*)#iu";
      echo "Street starts with number \n";
      preg_match($pattern, $street, $matches);
      $streetname = trim($matches[2]);
      $number = trim($matches[1]);
      
      if(preg_match("#([0-9]{1,}[a-zA-z]{0,3})$#i", $streetname)) {
        echo "Street starts and ends with number \n";
        preg_match("#^([a-z0-9 \-\/ \'.]*)[,]?[\s]*([a-z\#\.]{1,5}[\s]*[0-9]{1,5}[a-z0-9 \p{L} \-\/]{0,})#iu", $streetname, $res);
        
        $streetname = $res[1];
        $number = $number . ' ' . trim($res[2]);
      }
    }
    else {
      $spaceproblem = "#(\s?)(?P<devider>\/|\-)(\s?)#iu";
      preg_match($spaceproblem, $street, $matches);
      $devider  = $matches['devider'];
      $street = preg_replace($spaceproblem, $devider, $street);

      preg_match($pattern, $street, $matches);
      echo "Street ends with number \n";
      $streetname = $matches[1];
      $number = trim($matches[2] . (isset($matches[3]) ? $matches[3] : ''));
    }
    $streetname = trim($streetname);
    $number = trim($number);
    
    // Did we actually find anything at all
    if(empty($streetname) || empty($number)) {
      return false;
    }
    $number = str_replace(',', '', $number);
    return Array($streetname, $number, $additional);
  }
}
