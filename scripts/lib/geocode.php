<?
/**
 * This class will use Google Maps' service to validate adresses
 *
 * PHP version 5.3
 *
 * @author     Vic Degraeve <vicdegraeve@me.com>
 * @version    1.0
 */

class Geocode {  
  private function curl_file_get_contents($url) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $url);
    $contents = curl_exec($c);
    curl_close($c);
    if ($contents) return $contents;
      else return FALSE;
  }
  
  private function language($language, $store) {
    if(stripos('eyewishlashes', $store) !== false) {
      if(stripos('-EU', $store) !== false) {
        $storeISO = 'uk';
      }
      elseif(stripos('-UK', $store) !== false) {
        $storeISO =  'uk';
      }
      elseif(stripos('-US', $store) !== false) {
        $storeISO = 'us';
      }
    }
    else {
      if(stripos('wimpernwuensche', $store) !== false) {
        $storeISO = 'de';
      }
      elseif(stripos('desirsdecils', $store) !== false) {
        $storeISO = 'fr';
      }
      elseif(stripos('wimperwensen', $store) !== false) {
        $storeISO = 'nl';
      }      
    }

    if($language = 'ch') {
      switch ($storeISO) {
        case 'de':
          return 'de'; /* De return bepaalt de taal in getLocation */
          break;
        case 'uk':
          return 'de';
          break;
        case 'fr':
          return 'fr';
          break;
        default:
          return 'de'; /* Default taal van Zwitserland */
          break;
      }
    }


    if($language = 'be') {
      switch ($storeISO) {
        case 'nl':
          return 'nl'; /* De return bepaalt de taal in getLocation */
          break;
        case 'fr':
          return 'fr';
          break;
        default:
          return 'nl'; /* Default taal van België is Nederlands ofcourse */
          break;
      }
    }
  }

  private function getLocation($address, $language, $store) {
    $language = $this->language($language, $store);
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&language=".strtolower($language)."&address=". urlencode(html_entity_decode($address, ENT_NOQUOTES, 'UTF-8'));
    $json = $this->curl_file_get_contents($url);
    $response = json_decode($json, true);
    if($response['status'] == 'OK') {
      return $response['results'][0];
    }
    else {
      return false;  
    }  
  }
  
  public function getAddress($address, $language, $store) {
    $address = $this->getLocation($address, $language, $store);

    if($address == false) {
      return false;
    }

    $result = Array();
  
    foreach ($address['address_components'] as $component) {
      if(in_array('street_address', $component['types']) || in_array('route', $component['types'])) {
        $result['street'] = $component['long_name'];
      }
      elseif(in_array('street_number', $component['types'])) {
        $result['number'] = $component['long_name'];
      }
      elseif(in_array('postal_code', $component['types'])) {
        $result['zip'] = $component['long_name'];
      }
      elseif(in_array('administrative_area_level_1', $component['types'])) {
        $result['state'] = $component['long_name'];
      }
      elseif(in_array('locality', $component['types'])) {
        $result['city'] = $component['long_name'];
      }
    }
  
    return $result;
  }
}
