<?php
/**
 * UK:
 */

// Overrides
if(empty($order->order->ship_data->address->address3)) {
  $street = 'address1';
  $additional = 'address3';
}
elseif(!empty($order->order->ship_data->address->address1)) {
  $street = 'address3';
  $additional = 'address1';
}

/*
  If any fields in the xml are different than this:
    $legend = Array (
      'street' => 'address1',
      'number' => 'address2'  
    );
  Or you need to add extra variables, then you can add or edit
  like in this example:
  
  $legend['street'] = 'address1';
  $legend['number'] = 'address2';
*/

// Legend
$legend['street'] = $street;
$legend['additional'] = $additional;

// Replace
$replace_street = "false";