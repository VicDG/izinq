<?php
/**
 * EN:
 */

/*
  If any fields in the xml are different than this:
    $legend = Array (
      'street' => 'address1',
      'number' => 'address2'  
    );
  Or you need to add extra variables, then you can add or edit
  like in this example:
  
  $legend['street'] = 'address1';
  $legend['number'] = 'address2';
*/