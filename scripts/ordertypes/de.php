<?php
/**
 * DE:
 */

// Overrides
// Initiating postnummer
$packpattern = '/(?P<postnummer>postnummer)[\s]*(?P<number>[0-9]*)/i';
$postnummer = Array( Array(preg_match($packpattern, $s_address->address1, $results_address1), 'address1'), Array(preg_match($packpattern, $s_address->address2, $results_address2), 'address2'), Array(preg_match($packpattern, $s_address->company, $results_company), 'company')) ;

// Stripping out postnummers
foreach ($postnummer as $station) {
  if($station[0] == true) {
    $results = ${'results_' . $station[1]};
    $s_address->$station[1] = trim(preg_replace($packpattern, '', $s_address->$station[1]));
  }
}

// Saving postnummer
$postnummer = trim($results['number']);

// Try Again
if(empty($results['number'])) {
  $retry = preg_match('/([0-9\s]*)([a-z]*)/i', $s_address->company, $results);
  if(!empty($results[1]) && empty($results[2])) {
    $postnummer = trim($results[1]);
    $s_address->company = trim(str_replace($postnummer, '', $s_address->company));
  }
}

// Initiating packstations
$packpattern = '/(?P<station>packstation)[\s]*(?P<number>[0-9]*)/i';
$packstation = Array( Array(preg_match($packpattern, $s_address->address1, $results_address1), 'address1'), Array(preg_match($packpattern, $s_address->address2, $results_address2), 'address2'), Array(preg_match($packpattern, $s_address->company, $results_company), 'company')) ;

// Stripping out Packstations
foreach ($packstation as $station) {
  if($station[0] == true) {
    $results = ${'results_' . $station[1]};
    $s_address->$station[1] = trim(preg_replace($packpattern, '', $s_address->$station[1]));
  }
}

// Saving Packstation
$packstation = trim($results['number']);

// Compare numbers
if(($postnummer == $packstation) && (!empty($packstation) && !empty($postnummer)))  {
  $s_address->address1 = trim('Packstation ' . trim($postnummer) . ' ' . trim($s_address->address1));
}
elseif(!empty($packstation) && !empty($postnummer)) {
  $s_address->address1 = trim('Packstation ' . trim($postnummer) . ' ' . trim($s_address->address1));
  $s_address->address2 = trim($packstation);
}
elseif(!empty($packstation)) {
  $s_address->address1 = trim('Packstation ' . trim($packstation) . ' ' . trim($s_address->address1));
}
elseif(!empty($postnummer)) {
  $s_address->address1 = trim('Packstation ' . trim($postnummer) . ' ' . trim($s_address->address1));
}

/*
  If any fields in the xml are different than this:
    $legend = Array (
      'street' => 'address1',
      'number' => 'address2'  
    );
  Or you need to add extra variables, then you can add or edit
  like in this example:
  
  $legend['street'] = 'address1';
  $legend['number'] = 'address2';
*/