<?php
/**
 * NL:
 * NL + BE (VL + WL)
 */

/*
  If any fields in the xml are different than this:
    $legend = Array (
      'street' => 'address1',
      'number' => 'address2'  
    );
  Or you need to add extra variables, then you can add or edit
  like in this example:
  
  $legend['street'] = 'address1';
  $legend['number'] = 'address2';
*/
 
// Exceptions
$matches = Array();
if(preg_match('/^(\d\d\d\d)([a-zA-Z][a-zA-Z])/', $s_address->zip, $matches)) {
  $s_address->zip = $matches[1] . ' ' . strtoupper($matches[2]);
}