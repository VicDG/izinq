#!/bin/sh
# (POSIX shell syntax)
verbose=0

# Colors
RCol='\e[0m'    # Text Reset

# Regular           Bold                Underline           High Intensity      BoldHigh Intens     Background          High Intensity Backgrounds
Bla='\e[0;30m';     BBla='\e[1;30m';    UBla='\e[4;30m';    IBla='\e[0;90m';    BIBla='\e[1;90m';   On_Bla='\e[40m';    On_IBla='\e[0;100m';
Red='\e[0;31m';     BRed='\e[1;31m';    URed='\e[4;31m';    IRed='\e[0;91m';    BIRed='\e[1;91m';   On_Red='\e[41m';    On_IRed='\e[0;101m';
Gre='\e[0;32m';     BGre='\e[1;32m';    UGre='\e[4;32m';    IGre='\e[0;92m';    BIGre='\e[1;92m';   On_Gre='\e[42m';    On_IGre='\e[0;102m';
Yel='\e[0;33m';     BYel='\e[1;33m';    UYel='\e[4;33m';    IYel='\e[0;93m';    BIYel='\e[1;93m';   On_Yel='\e[43m';    On_IYel='\e[0;103m';
Blu='\e[0;34m';     BBlu='\e[1;34m';    UBlu='\e[4;34m';    IBlu='\e[0;94m';    BIBlu='\e[1;94m';   On_Blu='\e[44m';    On_IBlu='\e[0;104m';
Pur='\e[0;35m';     BPur='\e[1;35m';    UPur='\e[4;35m';    IPur='\e[0;95m';    BIPur='\e[1;95m';   On_Pur='\e[45m';    On_IPur='\e[0;105m';
Cya='\e[0;36m';     BCya='\e[1;36m';    UCya='\e[4;36m';    ICya='\e[0;96m';    BICya='\e[1;96m';   On_Cya='\e[46m';    On_ICya='\e[0;106m';
Whi='\e[0;37m';     BWhi='\e[1;37m';    UWhi='\e[4;37m';    IWhi='\e[0;97m';    BIWhi='\e[1;97m';   On_Whi='\e[47m';    On_IWhi='\e[0;107m';

# Help
Help (){
  echo -e "${BWhi}'$0 status'${RCol}      - ${Yel}Returns the status of the program" >&2
  echo -e "${BWhi}'$0 (-v) start'${RCol}  - ${Yel}Starts the program. If the -v flag is added in front of the parameter the output of the script will be displayed for debugging purposes." >&2
  echo -e "${BWhi}'$0 stop'${RCol}        - ${Yel}Stops the program${RCol}" >&2
}

# Cleanup
cleanup () {
  echo -e "${RCol}---";
}
trap cleanup EXIT

# Status
Status (){
  if [ -f pids/izinq.pid ] ; then
    pid=`cat "pids/izinq.pid"`
    echo -e "Process is ${Gre}running${RCol} with PID $pid" >&2 
  else
    echo -e "Process is ${Red}not running${RCol}" >&2 
  fi
}

# Start
Start (){
  Stop
  if [ $verbose = "1" ] ; then
    echo -e "Process is ${Gre}running${RCol}" >&2
    while true; do
      echo -e "---${BWhi}" >&2
      php scripts/task.php
      echo -e "${RCol}Loop complete, relooping in one minute${BWhi}" >&2
      sleep 60
    done
  else
    if [ ! -f pids/izinq.pid ] ; then
      chmod 755 daemon.sh
      path=`pwd`
      ./daemon.sh >> log.txt 2>&1 &
      echo $! > pids/izinq.pid
      echo -e "Process is ${Gre}running${RCol} with PID $!" >&2
      exit 0
    else
      echo -e "${BRed} Process already running!${RCol}" >&2
      exit 1
    fi
  fi
}

# Stop
Stop (){
  if [ -f pids/izinq.pid ] ; then
    pid=`cat "pids/izinq.pid"`
    kill "$pid" > /dev/null
    if [ -f pids/izinq.pid ] ; then
      kill -9 "$pid" > /dev/null
      rm -rf pids/izinq.pid 
    fi
    echo -e "Process was ${Red}stopped${RCol}" >&2
  else
    echo "Not running" >&2
  fi
}

while :
do
    case $1 in
        help | -h | --help | -\?)
            #  Call your Help() or usage() function here.
            Help
            exit 0      # This is not an error, User asked help. Don't do "exit 1"
            ;;
        -v | --verbose)
            # Each instance of -v adds 1 to verbosity
            verbose="1"
            shift
            ;;
        status)
        	Status
            exit 1
            ;;
        start)
        	Start
            exit 1
            ;;
        stop)
        	Stop
            exit 1
            ;;
        *)  # no more options. Stop while loop
            echo "WARN: Unknown option (ignored): $1" >&2
            echo "Use '$0 help' to see a list of options" >&2
            break
            ;;
    esac
done