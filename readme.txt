# Installatie
Wanneer je voor de eerste keer dit script installeert moet je allereerst zorgen dat je in de directory 'izinq' zit.
Hierna voer je 'chmod 755 izinq.sh' uit.
Zorg er ook voor dat php geinstalleerd is en werkt in de commandline.

# Uitvoeren
Nu het script de juiste permissies heeft kan je het gebruiken. Om een lijst met functies en commands te zien voer je './izinq.sh help' uit.

# Aanvullende informatie
De mappen waarin de XML's worden geplaatst en waaruit ze worden verwerkt moeten d.m.v. 'chmod 777' benodigde rechten worden gegeven.