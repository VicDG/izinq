#!/bin/sh
# (POSIX shell syntax)
# Cleanup
cleanup () {
  rm -rf pids/izinq.pid
}
trap cleanup EXIT
while true; do
  php scripts/task.php
  sleep 60
done